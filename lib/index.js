/** Checks to see if we are on Node
 *
 *  If not we assume it is a browser environment
 */
const is_browser = () => {
    try {
        return this === window
    }
    catch (e) {
        return false
    }
}


const check_node = (req) => {
    return req.headers['user-agent'].indexOf('BeakerBrowser') > -1
}


const check_browser = () => window.DatArchive != undefined


const has_dat = (req) => {
    return is_browser()? check_browser() : check_node(req)
}

if (!is_browser()) {
    module.exports = has_dat
}
