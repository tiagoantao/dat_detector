const express = require('express')

const has_dat = require('../../lib/index.js')

const app = express()

app.get('/',
	(req, res) => res.send('<h1>dat? ' + has_dat(req) + '</h1>'))

app.listen(8080)
