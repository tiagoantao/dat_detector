const koa = require('koa')

const has_dat = require('../../lib/index.js')

const app = new koa()

app.use(async ctx => {
    ctx.body = '<h1>dat? ' + has_dat(ctx.req) + '</h1>'
})

app.listen(8080)
