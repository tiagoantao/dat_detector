const http = require('http')

const has_dat = require('../../lib/index.js')

const run_server = () => {
    process.on('SIGINT', (code) => {
        console.log('Bye')
        process.exit()
    })

    http.createServer((req, res) => {
	res.end('<h1>dat? ' + has_dat(req) + '</h1>')
    }).listen(8080)
}

run_server()
