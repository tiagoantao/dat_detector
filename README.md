dat-detector
============


This very small module detects if the browser that you are using is
dat:/// enabled.

It allows you to provide tailored content for dat-enabled browsers,
both on the server- and client-sides.

Currently this is only [Beaker](https://beakerbrowser.com/), but I
intend to support whatever browers (and browser extensions) appear in
the future, so this module would be resilient and future-proof way of
knowing if dat is supported (as long as you update to a recent
version, that is).

# Usage

## Installation

You probably have your own process, but the usual stuff works

`npm install @tiagoantao/dat-detector`

### Browser usage

The simplest example possible:

```
<body onload="test_dat()">
  <script src="https://tiago.org/dat_detector/lib/index.js"></script>
  <p>Am I a dat-enabled browser? <span id="answer"></span></p>
  <script>
    const test_dat = () => {
        const answer = has_dat() ? "yes" : "no"
        document.getElementById("answer").innerHTML = answer
    }
  </script>
</body>		
```

I use this on <a href="https://tiago.org">my personal home page</a>.
You will see different results if you go there with Beaker or another
browser.

### Node.JS

I provide 3 examples:

- [Basic Node](examples/node)
- [Express](examples/express)
- [Koa](examples/koa)


# Author, Acknowledgments, License & etc

Author: [Tiago Antao](mailto:tiago@tiago.org)

License: Affero GPL 3.0

Thanks: To Tara Vancil to help me with Beaker's basic stuff
